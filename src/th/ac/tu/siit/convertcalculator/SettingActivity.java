package th.ac.tu.siit.convertcalculator;

import java.util.Locale;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class SettingActivity extends Activity implements OnClickListener {
	
	float exchangeRate, intRate;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_setting);
		
		Button b1 = (Button)findViewById(R.id.btnFinish);
		b1.setOnClickListener(this);
		
		Intent i = this.getIntent();
		exchangeRate = i.getFloatExtra("exchangeRate", 32.0f);
		
		
		EditText etRate = (EditText)findViewById(R.id.etRate);
		etRate.setText(String.format(Locale.getDefault(), "%.2f", exchangeRate));
		
		Intent it = this.getIntent();
		intRate = it.getFloatExtra("intRate", 10.0f);
		
		EditText int_box = (EditText)findViewById(R.id.int_box);
		int_box.setText(String.format(Locale.getDefault(), "%.2f", intRate));
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.setting, menu);
		return true;
	}

	@Override
	public void onClick(View v) {
		EditText etRate = (EditText)findViewById(R.id.etRate);
		try {
			float r = Float.parseFloat(etRate.getText().toString());
			Intent data = new Intent();//create intent to wrap return value
			data.putExtra("exchangeRate", r);//add the return value to the intent
			this.setResult(RESULT_OK, data);//set intent to result for passing back
			this.finish();//end the current activity
		} catch(NumberFormatException e) {
			Toast t = Toast.makeText(getApplicationContext(), 
					"Invalid exchange rate", Toast.LENGTH_SHORT);
			t.show();
			etRate.setText("");
			etRate.requestFocus();
		} catch(NullPointerException e) {
			Toast t = Toast.makeText(getApplicationContext(), 
					"Invalid exchange rate", Toast.LENGTH_SHORT);
			t.show();
			etRate.setText("");
			etRate.requestFocus();
		}
		
		EditText intRate = (EditText)findViewById(R.id.int_box);
		try {
			float r = Float.parseFloat(etRate.getText().toString());
			Intent data = new Intent();//create intent to wrap return value
			data.putExtra("exchangeRate", r);//add the return value to the intent
			this.setResult(RESULT_OK, data);//set intent to result for passing back
			this.finish();//end the current activity
		} catch(NumberFormatException e) {
			Toast t = Toast.makeText(getApplicationContext(), 
					"Invalid interest rate", Toast.LENGTH_SHORT);
			t.show();
			intRate.setText("");
			intRate.requestFocus();
		} catch(NullPointerException e) {
			Toast t = Toast.makeText(getApplicationContext(), 
					"Invalid interest rate", Toast.LENGTH_SHORT);
			t.show();
			intRate.setText("");
			intRate.requestFocus();
		}
	}

}
