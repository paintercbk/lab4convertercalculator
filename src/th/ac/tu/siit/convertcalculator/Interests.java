package th.ac.tu.siit.convertcalculator;

import java.util.Locale;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class Interests extends Activity implements OnClickListener{
	
	float intRate;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_interests);
		
		Button st = (Button)findViewById(R.id.button1);
		Button cal = (Button)findViewById(R.id.button2);
		
		st.setOnClickListener(this);
		cal.setOnClickListener(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.interests, menu);
		return true;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		int id = v.getId();
		
		if(id == R.id.button2)
		{
			EditText mon = (EditText)findViewById(R.id.editText1);
			TextView intrateTV = (TextView)findViewById(R.id.textView3);
			EditText timeET = (EditText)findViewById(R.id.editText2);
			TextView show = (TextView)findViewById(R.id.textView6);
			String res = "";
			try {
				double intrate = Float.parseFloat(intrateTV.getText().toString());
				double money = Float.parseFloat(mon.getText().toString());
				double time = Float.parseFloat(timeET.getText().toString());
				double urmon = (money* Math.pow(1.0+(intrate)/100.0, time));
				res = String.format(Locale.getDefault(), "%.2f", urmon);
			} catch(NumberFormatException e) {
				res = "Invalid input";
			} catch(NullPointerException e) {
				res = "Invalid input";
			}
			show.setText(res);
		}
		else if(id == R.id.button1)
		{
			Intent i = new Intent(this, SettingActivity.class);
		i.putExtra("intRate", 0.1);
		startActivityForResult(i, 9999);
		}
	}
	
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == 9999 && resultCode == RESULT_OK) {
			intRate = data.getFloatExtra("intRate", 10.0f);
			TextView tvRate = (TextView)findViewById(R.id.textView3);
			tvRate.setText(String.format(Locale.getDefault(), "%.2f", intRate));
		}
	}


}
